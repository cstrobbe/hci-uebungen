# hci-uebungen

Übungsdateien für HCI.

Autor: Christophe Strobbe.

Dieses Repository clonen: `git clone https://gitlab.com/cstrobbe/hci-uebungen`.

Und hier ist die [Website «live»](https://cstrobbe.gitlab.io/hci-uebungen/index.html).

Lizenz: [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
